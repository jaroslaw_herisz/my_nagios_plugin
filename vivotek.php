<?php 
//username and password of account
$username = trim($argv[2]);
$password = trim($argv[3]);
$host = $argv[1];
$args = $argv[4];

function check_error(&$ch, &$error, $host)
{
	if (curl_error($ch)){
	    $error[] = "Curl error: ".curl_error($ch);
	}
}

//Arrary of errors
$data = array();
$error = array();
$info = array();

//set the directory for the cookie using defined document root var
$dir = "/tmp";
//build a unique path with every request to store 
//the info per user with custom func. 
$path = $dir;

//login form action url
$url="http://".$host."/cgi-bin/admin/getparam.cgi"; 

$cookie_file_path = $path."/cookie.txt";

$ch = curl_init();
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_COOKIESESSION, true);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name');  //could be empty, but cause problems on some hosts
curl_setopt($ch, CURLOPT_COOKIEFILE, $path);  //could be empty, but cause problems on some hosts
curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
curl_setopt($ch, CURLOPT_USERPWD, $username.':'.$password);
////////////////////////////////////////////////LOGIN
curl_setopt($ch, CURLOPT_URL, $url);
$lines = array();
$lines = curl_exec($ch);
#Usówam smieciowe znaki ^M
$lines = str_ireplace("\x0D", "", $lines);
$lines = str_ireplace("'", "", $lines);
#dziele na linie
$lines = explode("\n", $lines);

check_error($ch, $error, $host);

foreach($lines as $line)if($line!="")
{
	$tmp = explode('=', $line); 
	$data[$tmp[0]] = $tmp[1];
}	
curl_close($ch);

////////////////////////////////////////////////Tablica zadan
$args = explode(",", $args);
foreach($args as $line)if($line!="")
{
	$line = explode("=", $line);
	if($line[0] == "time"){
		$parsed = date_parse_from_format("Y/m/d H:i:s", $data['system_date']." ".$data['system_time']);
		$timestap = mktime(
	        $parsed['hour'], 
	        $parsed['minute'], 
	        $parsed['second'], 
	        $parsed['month'], 
	        $parsed['day'], 
	        $parsed['year']
		);
		if(abs($timestap-time())<100)
			$info[] = "Time ".$data['system_date']." ".$data['system_time']." OK";
		else 
			$error[] = "Time ".$data['system_date']." ".$data['system_time']." ERROR";
		
	}
	else {
		if(isset($data[$line[0]]) && $data[$line[0]] = $line[1])
		{
			$info[] = $line[0]."=".$line[1]." OK";
		}else{
			$error[] = $line[0]."=".$line[1]." ERROR";
		}
	}
	
}

if(count($error)>0)
{
	foreach($error as $line )
	{
		echo $line. "; ";
	}
	exit(2);
} elseif(count($info)>0)
{
	foreach($info as $line )
	{
		echo $line. "; ";
	}
	exit(0);
} 
?>