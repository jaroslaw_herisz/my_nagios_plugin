<?php 
//username and password of account
$username = trim($argv[2]);
$password = trim($argv[3]);
$host = $argv[1];

function check_error(&$ch, &$error, $host)
{
	if (curl_error($ch)){
	    $error[] = "Curl error: ".curl_error($ch);
	}
	if(curl_getinfo($ch, CURLINFO_EFFECTIVE_URL)=="http://".$host."/unauth.htm")
	{
		$error[] = "CRITICAL: Thecus login failed";
	}
	if(curl_getinfo($ch, CURLINFO_EFFECTIVE_URL)=="http://".$host."/adm/inuse.htm")
	{
		$error[] = "CRITICAL: Admin has already logged in from another host";
	}
}

//Arrary of errors
$error = array();
$info = array();

//set the directory for the cookie using defined document root var
$dir = "/tmp";
//build a unique path with every request to store 
//the info per user with custom func. 
$path = $dir;

//login form action url
$url="http://".$host."/usr/usrgetform.html?name=index"; 
$postinfo = "username=".$username."&pwd=".$password;

$cookie_file_path = $path."/cookie.txt";

$ch = curl_init();
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $postinfo);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_COOKIESESSION, true);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name');  //could be empty, but cause problems on some hosts
curl_setopt($ch, CURLOPT_COOKIEFILE, $path);  //could be empty, but cause problems on some hosts
////////////////////////////////////////////////LOGIN
curl_setopt($ch, CURLOPT_URL, $url);
$answer = strip_tags(curl_exec($ch));
check_error($ch, $error, $host);

//////////////////////////////////////////////GET DISK DATA
$url="http://".$host."/adm/getform.html?name=disks";
//page with the content I want to grab
curl_setopt($ch, CURLOPT_URL, $url);
$answer = strip_tags(curl_exec($ch));
check_error($ch, $error, $host);
$answer = strip_tags($answer);
//wycinam wszystko między Disks Information a Disk Power Management
preg_match_all('/Disks Information(.*)Disk Power Management/s', $answer, $matches);
//mam blok infa o dyskach
$answer = $matches[1][0];
 
//szukam fragmetów psaujacych do w kolejnych liniach
//\d pozycja
//[\d,] pojemnosc
//[\dA-Za-z-] Model
//[\dA-Z] Firmware
//[a-zA-Z] Status
//pomiędzy \s*\n\s* tz spacje nowalinia spacje
preg_match_all('/\d+\s*\n\s*[\d,]+\s*\n\s*[\dA-Za-z-]+\s*\n\s*[\dA-Z]+\s*\n\s*[a-zA-Z]+/s', $answer, $matches);

//Mam info o dyskach
$answer = $matches[0];

//przechodze po kolejnych dyskach
foreach ($answer as $dyski)
{
	//czyszcze brzydkie spacje na poczatku
	$dyski =explode("\n", str_replace(" ", '', $dyski));
	
	//Sprawdzam statys
	if($dyski[4] == "Detected")
	{
		//loguje info jak dysk Detected
		$info[] = $dyski[0] ." ". $dyski[1] ." ". $dyski[2] ." ". $dyski[3] ." ". $dyski[4];
	}
	else
	{
		//loguje error jak jestcoś innego niż Detected
		$error[] = $dyski[0] ." ". $dyski[1] ." ". $dyski[2] ." ". $dyski[3] ." ". $dyski[4];
	}
}


//////////////////////////////////////////////LOGOUT
$url="http://".$host."/adm/logout.html";
//page with the content I want to grab
curl_setopt($ch, CURLOPT_URL, $url);
$answer = strip_tags(curl_exec($ch));
check_error($ch, $error, $host);

curl_close($ch);
if(count($error)>0)
{
	foreach($error as $line )
	{
		echo "CRITICAL: ".$line. ", ";
	}
	exit(2);
} elseif(count($info)>0)
{
	foreach($info as $line )
	{
		echo $line. ", ";
	}
	exit(0);
} 
?>